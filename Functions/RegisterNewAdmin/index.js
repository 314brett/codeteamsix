exports.RegisterNewAdmin = (req, res) => {
  const request = require('request');
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET, POST')
  let facebookAppID = '2221773394545898';
  let facebookAppSecret = '10b78ddd71d5525da28a26b02c557a33';
  const {Storage} = require('@google-cloud/storage');
  const storage = new Storage();
  const bucketName = 'democracy-voucher-create-admin-code';
  const destFilename = '/tmp/admin-file';
  var fs = require('fs');

  let userToken = req.body.token;
  let userRegistrationCode = req.body.registrationCode;

  let url = 'https://graph.facebook.com/oauth/access_token?client_id=' + facebookAppID + '&client_secret=' + facebookAppSecret + '&grant_type=client_credentials';
  request(url, { json: true }, (err, fbRes, body) => {
    if (err) { return console.log(err); }
    let serverAppToken = body.access_token;
    console.log("body1:");
    console.log(body);
    let link = 'https://graph.facebook.com/debug_token?input_token=' + userToken + '&access_token=' + serverAppToken;
    console.log("link: " + link);
    request(link, { json: true }, (err, fbRes, body) => {
      if (err) { return console.log(err); }

      console.log("body2:");
      console.log(body);

      if(body.data.is_valid===true && body.data.app_id===facebookAppID) {

        let userID = body.data.user_id;

        makeNewAdmin(userID, userRegistrationCode, res);

      }
    });
  });

  function makeNewAdmin(userID, userRegistrationCode, res){

    let srcFilename = userRegistrationCode;
    const options = {
      destination: destFilename,
    };
    storage
        .bucket(bucketName)
        .file(srcFilename)
        .download(options)
        .then(function(info){
          //registration code verified

          //add admin to bucket
          fs.writeFile("/tmp/" + userID, "", function(err) {
            if(err) {
              return console.log(err);
            }

            let bucket = 'democracy-vouchers-admin-accounts';
            storage.bucket(bucket).upload("/tmp/" + userID).then(() => {
              res.send("continuing");
            }).catch((err) => {
              console.log("err: " + err);
            });

            storage.bucket(bucketName).file(userRegistrationCode).delete().then(() => {

              res.send("admin account added");

            }).catch((err) => {
              console.log(err);
            });

            console.log("The file was saved!");
          });



        }).catch(function(err){
          console.log(err);
      res.send("unauthorized: " + err);
    });
  }


};

