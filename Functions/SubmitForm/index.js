const {Storage} = require('@google-cloud/storage');
const {Datastore} = require('@google-cloud/datastore');
const projectId = 'democracy-vouchers';
var dateFormat = require('dateformat');
const uuidv4 = require('uuid/v4');

const storage = new Storage({
    projectId: projectId,
    keyFilename: './gcloud-key.json'
});
const datastore = new Datastore({
    projectId: projectId,
});

var myBucket = storage.bucket('democracy-vouchers-uploaded');

exports.SubmitForm = (req, res) => {
    res.set('Access-Control-Allow-Origin', "*");
    res.set('Access-Control-Allow-Methods', 'GET, POST');

    console.log("req body");
    console.log(req);
    console.log("2");
    console.log(JSON.stringify(req.body));

    let email = req.body.Email;
    let firstName = req.body.Firstname;
    let lastName = req.body.LastName;
    let submissionID = req.body.SubmissionID;
    let totalImages = req.body.TotalImages;
    let extension = req.body.Extension;

    console.log("Extension:" + extension);
    if (totalImages > 8) {
        totalImages = 8;
    }

    let expiresDate = getExpiresDate();

    let policies = [];

    console.log("totalImages: " + totalImages);

    for (let i = 0; i < totalImages; i++) {
        var file = myBucket.file(submissionID + "-image" + i +"."+ extension);

        var options = {
            equals: ['$Content-Type', 'image/*'],
            expires: expiresDate
        };
        console.log("getSignedPolicy " + i);
        file.getSignedPolicy(options, function (err, policy) {
            // policy.string: the policy document in plain text.
            // policy.base64: the policy document in base64.
            // policy.signature: the policy signature in base64.

            if (err) {
                console.log("getSignedPolicy error: " + err);
            } else {
                policies.push(policy);

                //Comparing string to int
                if (policies.length == totalImages && validFormData(firstName,lastName,email,totalImages)) {
                    submitToDatastore(firstName, lastName, email, policies, totalImages, submissionID, res, extension);
                }
            }

        });
    }


};

function validFormData(firstName, lastName, email, totalImages)
{
    //If all fields exist and there are an even number of images
    if (typeof(firstName) !== 'undefined' && typeof(lastName)
        !== 'undefined' && typeof(email) !== 'undefined' && (totalImages > 0 && (totalImages % 2) === 0))
    {
        //if the text fields actually have data in them, return true
        if (firstName.length > 0 && lastName.length > 0 && email.length > 0)
        {
            return true;
        }
    }
    else //if any field is missing, return false
    {
        return false
    }
}

function getExpiresDate() {
    let expires = new Date();
    //Upload link expires after x days
    let x = 7;
    expires.setDate((new Date()).getDate() + x);
    console.log("expires date: " + expires);

    let value = dateFormat(expires, "mm-dd-yyyy");
    console.log("expires " + value);
    return value;
}

function submitToDatastore(firstName, lastName, email, policies, totalImages, submissionID, res, extension) {
    let imageLinksArray = [];
    for (let m = 0; m < totalImages; m++) {
        imageLinksArray.push(
            "https://storage.googleapis.com/democracy-vouchers-uploaded/" + submissionID + "-image" + m +"."+ extension
        );
    }
    uploadFieldsToCloudDatastore(firstName, lastName, email, imageLinksArray, function () {
        res.send(policies);
    });
}

function uploadFieldsToCloudDatastore(firstName, lastName, email, arrayOfImageLinks, returnFunction) {
    let entityKind = 'Submission';
    let entityName = uuidv4();
    let uploadId = entityName;
    let entityKey = datastore.key([entityKind, entityName]);
    let imageLinks = {"values": []};

    console.log("uploadFieldsToCloudDatastore");

    for (let i = 0; i < arrayOfImageLinks.length; i++) {
        imageLinks.values.push(
            {
                "link": arrayOfImageLinks[i]
            }
        );
    }

    const entity = {
        key: entityKey,
        data: {
            personName: firstName + " " + lastName,
            email: email,
            imageLinks: arrayOfImageLinks,
            uploadId: uploadId,
            date: (new Date())
        },
    };

    datastore.save(entity, function (err) {
        if (!err) {
            // Record saved successfully.
            returnFunction();
        }
    });

}