const {Storage} = require('@google-cloud/storage');
const {Datastore} = require('@google-cloud/datastore');
const projectId = 'democracy-vouchers';

const storage = new Storage({
    projectId: projectId,
    keyFilename: './gcloud-key.json'
});

const datastore = new Datastore({
    projectId: projectId,
});

exports.GetVouchers = (req, res) => {
    res.header('Access-Control-Allow-Origin', 'https://storage.googleapis.com');
    getAllFieldsFromDatabase(req, res)
};

function getAllFieldsFromDatabase(req, res) {
    const query = datastore
        .createQuery('Submission');
    datastore.runQuery(query, function (err, results) {
        results.sort(compare);
        res.send(results)
    });
}

function compare(a,b) {
    if(a.date===null || b.date===null){
        return -1;
    }
    let aDate = new Date(a.date);
    let bDate = new Date(b.date);
    if (aDate < bDate)
        return -1;
    if (aDate > bDate)
        return 1;
    return 0;
}
