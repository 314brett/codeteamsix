exports.IsLoginTokenValid = (req, res) => {
  const request = require('request');
  res.set('Access-Control-Allow-Origin', "*");
  res.set('Access-Control-Allow-Methods', 'GET, POST');
  let facebookAppID = '$FACEBOOK_APP_ID';
  let facebookAppSecret = '$FACEBOOK_APP_SECRET';//expired
  const {Storage} = require('@google-cloud/storage');
  const storage = new Storage();
  const bucketName = 'democracy-vouchers-admin-accounts';
  const destFilename = '/tmp/admin-file';

  let userToken = req.query.token;

  let url = 'https://graph.facebook.com/oauth/access_token?client_id=' + facebookAppID + '&client_secret=' + facebookAppSecret + '&grant_type=client_credentials';
  request(url, { json: true }, (err, fbRes, body) => {
    if (err) { return console.log(err); }
    let serverAppToken = body.access_token;
    let link = 'https://graph.facebook.com/debug_token?input_token=' + userToken + '&access_token=' + serverAppToken;
    console.log("link: " + link);
    request(link, { json: true }, (err, fbRes, body) => {
      if (err) { return console.log(err); }

      if(body.data.is_valid===true && body.data.app_id===facebookAppID) {

        let userID = body.data.user_id;
        let srcFilename = userID;

        const options = {
          destination: destFilename,
        };
        storage
            .bucket(bucketName)
            .file(srcFilename)
            .download(options)
            .then(function(info){

              res.send("validated");

            }).catch(function(err){
          res.send("unauthorized");
        });


      }
    });
  });


};

