const {Storage} = require('@google-cloud/storage');
const {Datastore} = require('@google-cloud/datastore');
const projectId = 'democracy-vouchers';
const request = require('request');

const storage = new Storage({
    projectId: projectId,
    keyFilename: './gcloud-key.json'
});

const datastore = new Datastore({
    projectId: projectId,
});

exports.AdminChangeData = (req, res) => {
    res.header('Access-Control-Allow-Origin', 'https://storage.googleapis.com');
    let token = req.body.token;

    request.get({url: "https://us-central1-democracy-vouchers.cloudfunctions.net/IsLoginTokenValid"
        , qs: {token: token}}, function(err, response, body) {
        if(body==='validated'){
            doChange(req, res)
        }
    });
};

function doChange(req, res){
    let change = req.body.change;
    if(change==='archive' || change === 'unarchive'){
        let uploadId = req.body.uploadId;

        const query = datastore.createQuery('Submission');
        query.filter('uploadId', uploadId);

        const key = datastore.key(['Submission', uploadId]);

        datastore.runQuery(query, function (err, results) {
            let submissionData = results[0];
            submissionData.archived = change==='archive';

            const updatedSubmissionData = {
                key: key,
                data: submissionData
            };

            datastore.update(updatedSubmissionData, function(err, response){
                res.send(response);
            });

        });


    }
}

function getAllFieldsFromDatabase(req, res) {
    const query = datastore
        .createQuery('Submission');
    datastore.runQuery(query, function (err, results) {
        res.send(results)
    });
}