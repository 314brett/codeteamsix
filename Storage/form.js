var submissionID;

function setupFormSubmission() {

    $("#submitVoucherForm").submit(function (e) {
        let validated = validate();

        if(validated) {
            var form = $(this);
            var url = form.attr('action');
            var data = {};

            setImageURL();
            data.Email = $('#Email').val();
            data.Firstname = $('#FirstName').val();
            data.LastName = $('#LastName').val();
            data.SubmissionID = submissionID;
            data.TotalImages = getImageCount();
            data.Extension = $("#frontURL").val();


            console.log("submitting:");
            console.log(data);

            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    gotPoliciesNowUpload(data);
                }
            });

        }

        e.preventDefault(); // avoid to execute the actual submit of the form.
    });
}

function validate(){

    let valid = true;
    let totalImages = 0;

    let fronts = [];
    let backs = [];
    for(let i=1; i<=4; i++){
        fronts.push(document.getElementById(i + '-front'));
        backs.push(document.getElementById(i + '-back'));
    }

    for(let i=0; i<4; i++){
        let howManyOfTheseTwoHaveImages = 0;
        if(fronts[i].files && fronts[i].files[0]){
            howManyOfTheseTwoHaveImages += 1;
            totalImages += 1;
        }
        if(backs[i].files && backs[i].files[0]){
            howManyOfTheseTwoHaveImages += 1;
            totalImages += 1;
        }

        if(howManyOfTheseTwoHaveImages==1){
            $('#' + (i+1) + '-error').show();
            console.log("showing");
            valid = false;
        }

    }

    if(totalImages==0){
        valid = false;
        $('#form-error').show();
    }

    return valid;

}

function makeSubmissionID() {
    return (Date.now()) + "-" + Math.floor(Math.random() * 100000);
}

window.addEventListener('load', function () {
    setupVoucherUploadInputs();
});

//upload another set of vouchers
var totalFileUploadsShown = 1;

function addFile() {
    if (totalFileUploadsShown < 4) {
        totalFileUploadsShown++;
        $('#' + totalFileUploadsShown + '-selectImages').show();
    }
    if(totalFileUploadsShown === 4){
        $('#addVoucher').hide();
    }
}

function cloneVoucherUpload() {

    for (let i = 2; i <= 4; i++) {
        var html = $('#1-selectImages').clone();
        html.find('#1-front').attr('id', i + '-front');
        html.find('#1-back').attr('id', i + '-back');
        html.find('#1-frontVoucherImg').attr('id', i + '-frontVoucherImg');
        html.find('#1-backVoucherImg').attr('id', i + '-backVoucherImg');

        html.find('#1-front-clear').attr('id', i + '-front-clear');
        html.find('#1-back-clear').attr('id', i + '-back-clear');

        html.find('#1-error').attr('id', i + '-error');

        html.attr('id', i + '-selectImages');
        html.hide();
        $('#selectImagesHolder').append(html);
    }
}

function setupVoucherUploadInputs() {
    cloneVoucherUpload();
    for (let i = 1; i <= 4; i++) {
        document.getElementById(i + "-back").addEventListener('change', function () {
            if (this.files && this.files[0]) {
                var img = document.getElementById(i + '-backVoucherImg');
                img.src = URL.createObjectURL(this.files[0]); // set src to file url
                $("#" + i + "-back-clear").show();
                $("#" + i + "-back").hide();
                $('#' + i + '-error').hide();
                hideFormError();
            }
        });
        document.getElementById(i + "-front").addEventListener('change', function () {
            if (this.files && this.files[0]) {
                var img = document.getElementById(i + '-frontVoucherImg');
                img.src = URL.createObjectURL(this.files[0]); // set src to file url
                $("#" + i + "-front-clear").show();
                $("#" + i + "-front").hide();
                $('#' + i + '-error').hide();
                hideFormError();
            }
        });
        $("#" + i + "-front-clear").click(function(){
            document.getElementById(i + "-front").value = null;
            $("#" + i + "-front-clear").hide();
            $("#" + i + "-front").show();
            var img = document.getElementById(i + '-frontVoucherImg');
            $('#' + i + '-error').hide();
            img.src = "";
        });
        $("#" + i + "-back-clear").click(function(){
            document.getElementById(i + "-back").value = null;
            $("#" + i + "-back-clear").hide();
            $("#" + i + "-back").show();
            var img = document.getElementById(i + '-backVoucherImg');
            $('#' + i + '-error').hide();
            img.src = "";
        });
    }
    submissionID = makeSubmissionID();
    $('#addVoucher').click(function () {
        addFile();
    });
    setupFormSubmission();
}

function getImageCount() {
    let count = 0;
    $('.form-control-file').each(function (i, obj) {
        if (obj.files.length > 0) {
            count += 1;
        }
    });
    console.log("count: " + count);
    return count;
}

function gotPoliciesNowUpload(policies) {

    let totalImagesSelected = 0;
    $('.form-control-file').each(function (i, obj) {

        if (obj.files.length > 0) {

            let policy = policies[totalImagesSelected];
            console.log("policy:");
            console.log(policy);
            let policyJSON = JSON.parse(policies[totalImagesSelected].string);

            let bucket = policyJSON.conditions[1].bucket;
            let key = policyJSON.conditions[0][2];
            console.log("key: " + key);

            let url = 'https://storage.googleapis.com/' + bucket;
            console.log("url: " + url);

            var fd = new FormData();
            fd.append( 'key', key );
            fd.append( 'bucket', bucket );
            fd.append( 'Content-Type', policyJSON.conditions[2][2] );
            fd.append( 'GoogleAccessId', 'lambda-democracy-vouchers@democracy-vouchers.iam.gserviceaccount.com' );
            fd.append( 'policy', policy.base64 );
            fd.append( 'signature', policy.signature );
            fd.append( 'file', obj.files[0] );

            console.log("Post data2:");
            console.log(fd);

            $.ajax({
                url: url,
                type: 'POST',
                data: fd,
                success: function (data) {
                    console.log("response data: " + data);
                    $('#submitVoucherForm').hide();
                    $('#uploadSuccess').show();
                },
                cache: false,
                contentType: false,
                processData: false
            });

            totalImagesSelected += 1;
        }

    });


}


function hideFormError(){
    $('#form-error').hide();
}

function setImageURL() {
    var extension = document.getElementById("frontURL");
    var image = document.getElementById("1-front").value;
    extension.value = image.split('.')[1];


}