var hasBeenValidated = false;
var showingArchivedVouchers = false;

function notValidated(){
    document.getElementById("register-form-div").classList.remove('d-none');
    hideLoading();
}
function validated(){
    hasBeenValidated = true;
    document.getElementById("adminContent").classList.remove('d-none');
    hideLoading();

    loggedIn();
}

function loggedIn(){
    let token = FB.getAuthResponse()['accessToken'];

    // User is an admin


    //Call functions to download protected data
    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let results = xhttp.responseText;
            console.log (results);
            let usableData = JSON.parse(results);
            console.log(usableData[1]);
            let table = document.getElementById('table');
            let tableIndex = 0;
            usableData.forEach((resident) => {
                let row = table.insertRow(tableIndex+1);
                let email = row.insertCell(0);
                let images = row.insertCell(1);
                let name = row.insertCell(2);
                let archiveButtonHolder = row.insertCell(3);
                email.innerHTML = usableData[tableIndex]["email"];
                name.innerHTML = usableData[tableIndex]["personName"];

                let uploadId = usableData[tableIndex].uploadId;

                row.id = uploadId;

                let archiveButton = addArchiveButtonToHolder(archiveButtonHolder);

                if(usableData[tableIndex].archived){
                    document.getElementById(uploadId).className += " archived";
                    setButtonToArchived(archiveButton);
                }else{
                    document.getElementById(uploadId).className += " notArchived";
                    setButtonToUnArchived(archiveButton);
                }

                function setupArchiveButton(uploadId, archiveButton) {
                    archiveButton.onclick = function () {
                        if(showingArchivedVouchers) {
                            unarchive(uploadId, archiveButton);
                        }else{
                            archive(uploadId, archiveButton);
                        }
                    };
                }
                setupArchiveButton(uploadId, archiveButton);


                let imagesHTML = "";
                let imagesString = usableData[tableIndex]["imageLinks"];
                console.log("imagesString: " + imagesString);
                var imageArray = ("" + imagesString).split(',');
                console.log("imageArray: " + imageArray);
                for(let i=0; i<imageArray.length; i++){
                    console.log("i: " + i);
                    imagesHTML += "<a href='" + imageArray[i] + "' target='_blank'><img name='" + uploadId + "-" + i + "' class='voucherImg img-thumbnail' src='" + imageArray[i] + "'></a>";
                }
                images.innerHTML = imagesHTML;


                tableIndex++;
            });
        }
    };
    xhttp.open("GET", "https://us-central1-democracy-vouchers.cloudfunctions.net/GetVouchers", true);
    xhttp.send();


}


function showLoading(){
    document.getElementById("adminLoading").style.display = "block";
}
function hideLoading(){
    document.getElementById("adminLoading").style.display = "none";
}

var checkLoginStatusInterval;
function checkOnFacebookStatus(response){

    console.log(response);

    if(response.status==='connected'){
        clearInterval(checkLoginStatusInterval);

        //User still needs to be verified as an admin
        showLoading();
        connectedSuccessfully(response);
    }
    if(response.status==='unknown'){
        //User still needs to sign in
        hideLoading();
    }

}

function connectedSuccessfully(response){

    console.log("token: " + response.authResponse.accessToken);
    let token = response.authResponse.accessToken;

    $.ajax({
        url: "https://us-central1-democracy-vouchers.cloudfunctions.net/IsLoginTokenValid",
        type: "get", //send it through get method
        data: {
            token: response.authResponse.accessToken
        },
        success: function(response) {
            //Do Something
            if(response=="unauthorized"){
                notValidated();
            }
            if(response=="validated"){
                if(!hasBeenValidated) {
                    validated();
                }
            }
        },
        error: function(xhr) {
            //Do Something to handle error
            console.log("error: " + xhr);
        }
    });


}



window.onload = function(){
    checkLoginStatusInterval = setInterval(function() {FB.getLoginStatus(checkOnFacebookStatus)}, 500);
};

$(function() { //shorthand document.ready function

    $('#archivedButton').click(function(){
        showingArchivedVouchers = !showingArchivedVouchers;

        if(showingArchivedVouchers){
            showArchivedVouchers();
        }else{
            hideArchivedVouchers();
        }


    });

    $('#register-form').on('submit', function(e) { //use on if jQuery 1.7+
        showLoading();
        let token = FB.getAuthResponse()['accessToken'];
        e.preventDefault();  //prevent form from submitting
        let registrationCode = $('#registration-code').val();

        $.post("https://us-central1-democracy-vouchers.cloudfunctions.net/RegisterNewAdmin",
            {
                "registrationCode":registrationCode,
                "token":token
            },

            function(response){
                console.log("registered response: " + response);
                if(response==="admin account added"){

                    //hide the registration form
                    document.getElementById("register-form-div").style.display = "none";
                    if(!hasBeenValidated) {
                        validated();
                    }
                }
            });

    });
});

function addArchiveButtonToHolder(archiveButtonHolder){
    let button = document.createElement("BUTTON");
    var text = document.createTextNode("Archive");
    button.appendChild(text);
    archiveButtonHolder.appendChild(button);
    return button;
}

function showArchivedVouchers(){
    $('#archivedButton').removeClass("btn-light");
    $('#archivedButton').addClass("btn-primary");
    $('.archived').show();
    $('.notArchived').hide();
}
function hideArchivedVouchers(){
    $('#archivedButton').removeClass("btn-primary");
    $('#archivedButton').addClass("btn-light");
    $('.archived').hide();
    $('.notArchived').show();
}
function archive(uploadId, archiveButton){
    $("#" + uploadId).addClass("archived");
    $("#" + uploadId).removeClass("notArchived");
    if(showingArchivedVouchers){
        $("#" + uploadId).show();
    }else{
        $("#" + uploadId).hide();
    }

    setButtonToArchived(archiveButton);

    $.post("https://us-central1-democracy-vouchers.cloudfunctions.net/AdminChangeData",
        {
            change: "archive",
            uploadId: uploadId,
            token: FB.getAuthResponse()['accessToken']
        },
        function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
        });

}
function unarchive(uploadId, archiveButton){
    $("#" + uploadId).addClass("notArchived");
    $("#" + uploadId).removeClass("archived");
    if(showingArchivedVouchers){
        $("#" + uploadId).hide();
    }else{
        $("#" + uploadId).show();
    }

    setButtonToUnArchived(archiveButton);

    $.post("https://us-central1-democracy-vouchers.cloudfunctions.net/AdminChangeData",
        {
            change: "unarchive",
            uploadId: uploadId,
            token: FB.getAuthResponse()['accessToken']
        },
        function(data, status){
            console.log("Data: " + data + "\nStatus: " + status);
        });
}

function setButtonToArchived(button){
    button.innerHTML = "archived"
}
function setButtonToUnArchived(button){
    button.innerHTML = "archive";
}